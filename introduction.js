// let dog = {
//     name: 'Disco',
//     breed: 'Westy',
//     birthdate: '2008',
//     personnality: 'nice',
//     size: 'medium'
// };

// let dog2 = {
//     name: 'Khiba',
//     breed: 'Hoover',
//     birthdate: '2006',
//     personnality: 'player',
//     size: 'large'
// }



let target = document.querySelector('#target');

// newDog(dog);
// newDog(dog2);

createDog('Disco', 'Westy', '2008', 'nice', 'medium');

createDog('Khiba', 'Hoover', '2006', 'player', 'large');

createDog('Witch', 'JackRussel', '2012', 'crazy', 'small');

function createDog(name, breed, birthdate, personnality, size) {
    let firstDog = {
        name: name,
        breed: breed,
        birthdate: birthdate,
        personnality: personnality,
        size: size
    };
    newDog(firstDog);
};


function newDog(paramDog) {
    let newDog = document.createElement('p');
    newDog.textContent = `Hello I\'m ${paramDog.name}, I\'m a ${paramDog.personnality} dog, and I was born in ${paramDog.birthdate}. I\'m a ${paramDog.size} ${paramDog.breed}.`;
    target.appendChild(newDog);
};