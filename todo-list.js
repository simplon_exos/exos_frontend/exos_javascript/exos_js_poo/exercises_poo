class TodoList {
    constructor() {
        this.tasks = [];
    }
    addTask(description) {
        let newTask = new Task(description);
        this.tasks.push(newTask);
    }
    clearDone() {
        // on filtre l'array tasks pour supprimer toutes les entrées dont le done = true
        this.tasks = this.tasks.filter(task => task.done === false);

        console.log(this.tasks);
    }
    draw() {
        let target = document.querySelector('#to-do-list');
        let ul = document.createElement('ul');
        target.appendChild(ul);

        for (const task of this.tasks) {
            let newTask = task.draw();
            ul.appendChild(newTask);
        }
        return ul;
    }

}