class Task {
    constructor(label) {
        this.label = label;
        this.done = false;
    }

    toggleDone() {
        this.done = !this.done;
    }

    draw() {
        let li = document.createElement('li');
        li.textContent = this.label;

        let checkBox = document.createElement('input');
        checkBox.type = 'checkbox';
        li.appendChild(checkBox);


        if (this.done) {
            checkBox.checked = true;
        }
        checkBox.addEventListener('input', () => {
            this.toggleDone();
        })


        return li;

    }
}