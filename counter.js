// créer une classe Counter qui aura dans son constructeur
// une propriété value initialisée à 0 par défaut

// rajouter une méthode increment() qui viendra 
// ajouter 1 à la propriété value de la classe

class Counter {
    constructor() {
        this.value = 0;
    }

    increment(Counter) {
        this.value += 3;
    }

    decrement(Counter) {
        this.value--;
    }


    reset(Counter) {
        this.value = 0;
    }
}